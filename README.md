# Dialpad Rec Fetcher

## Description
This repo contains scripts necessary to download audio recordings from dialpad based on call centers

## Overall "how it works"

This uses the dialpad stats API to get a list of recordings, the API rquires a list of call cebters. 
https://developers.dialpad.com/reference/statscreate

## How to run it

1. Make sure you have an .env file with the following attribute DIALPAD_API_KEY=<>
2. Run **fetch_recordings_records.py** to download the recording medata from all SM callcenters
3. That will put the metadatafiles in an ./output directory
4. Once those are generated, you can run fetch_videos.py and it will walk through all the metadata files and will download all videos
5. The mp3's will be saved undel ./output_mp3_files, creating a subdir per call center
6. The output from the video download run will be saved in a status file per call center




