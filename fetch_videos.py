from dotenv import load_dotenv, find_dotenv
import os
import csv
import re
import requests

OUTPUT_MP3_DIR = "./output_mp3_files"

def read_files_from_directory(directory: str) -> list:
    """
    Load all the files from the specific directory tha conform to our output
    file format
    :param directory:
    :return:
    """
    matching_filenames = []
    # pattern = r'^\d{4}-\d{2}-\d{2}_ID\d+_[\w-]+_DaysAgo\d+\.csv$'  # Regex pattern
    pattern = r'^\d{4}-\d{2}-\d{2}_ID\d+_[\w()\-]+_DaysAgo900\.csv$'
    regex = re.compile(pattern)

    for filename in os.listdir(directory):
        if regex.match(filename):
            matching_filenames.append(filename)

    return matching_filenames


def read_csv_for_fields(file_path, fields):
    """
    Will read the specific fields from the csv spcecified
    :param file_path:
    :param fields:
    :return:
    """
    data = []

    with open(file_path, mode='r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            selected_data = {field: row[field] for field in fields if field in row}
            data.append(selected_data)

    return data


def get_urls_from_file(directory: str, filename: str) -> list:
    """
    Create a simple list of dicts that will have the call id and the url to the
    recording extracted from the file
    :param directory:
    :param filename:
    :return:
    """
    print(f"Read from file: {filename}")
    file_path = os.path.join(directory, filename)
    all_data = []
    with open(file_path, mode='r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            all_data.append(row)


def download_recordings(token: str,
                        recordings_list: list,
                        download_dir: str,
                        status_file) -> None:
    """
    Download the actual mp3's using the API token
    :param token:
    :param recordings_list:
    :param download_dir:
    :param status_file:
    :return:
    """

    if not os.path.exists(download_dir):
        os.makedirs(download_dir)

    print("Starting Downloads...")

    for recording in recordings_list:
        url = recording['recording_url']
        print(f"Downloading {url}")
        response = requests.get(url, headers={"Authorization": f"Bearer {token}", "accept": "application/json"},
                                stream=True)

        if response.status_code == 200:
            file_path = os.path.join(download_dir, recording['call_id'] + ".mp3")
            with open(file_path, 'wb') as file:
                for chunk in response.iter_content(chunk_size=128):
                    file.write(chunk)
            line = f"{recording['call_id']},{recording['recording_url']},{file_path}\n"
        else:
            # this is a bit hokey, but works for now
            line = f"error,{recording['recording_url']},{file_path}\n"

        status_file.write(line)


    print("Downloads Complete!")


def extract_id_and_name_from_filename(filename):
    """
    Extract the id and call center name from the formetterd file name
    :param filename:
    :return:
    """
    # Regular expression to match the pattern in the filename
    pattern = r'ID(\d+)_([\w()\-]+)_DaysAgo\d+\.csv'
    match = re.search(pattern, filename)

    if match:
        call_center_id = match.group(1)
        call_center_name = match.group(2).replace('-', ' ').replace('_', ' ')
        return call_center_id, call_center_name
    else:
        return None, None


if __name__ == '__main__':
    # load api configuration
    _ = load_dotenv(find_dotenv())  # read local .env file
    dialpad_api_key = os.getenv("DIALPAD_API_KEY")

    directory_to_read = './output_12042023'

    medatadatafiles = read_files_from_directory(directory_to_read)

    for file in medatadatafiles:
        urls = get_urls_from_file

        file_path = f"{directory_to_read}/{file}"
        fields = ['call_id', 'recording_url']
        url_list = read_csv_for_fields(file_path, fields)

        call_center_id, call_center_name = extract_id_and_name_from_filename(file)
        output_dir = os.path.join(OUTPUT_MP3_DIR, call_center_name)

        status_file_path = os.path.join(OUTPUT_MP3_DIR, f"status_{call_center_name}.txt")
        if os.path.exists(status_file_path):
            os.remove(status_file_path)
        status_file = open(status_file_path, 'w')

        download_recordings(dialpad_api_key,
                            url_list,
                            output_dir,
                            status_file)


    print(medatadatafiles)
