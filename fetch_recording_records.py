from dotenv import load_dotenv, find_dotenv
import os
import requests
import csv
from io import StringIO
import datetime
import time

"""
This script creates lists of recording files to download, based on the last 365 days
"""

OUTPUT_DIR = "./output/"


class CallCenter:
    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __repr__(self):
        return f"CallCenter(id={self.id}, name='{self.name}')"


def fetch_recording_metadata(token: str,
                             call_center_id: int,
                             days_ago: int) -> list:
    """
    Fetch recordings by doing the following:
    - Creating a stats request
    - Fetching the csv url with the request
    - Turn CSV into a list of dictionaries

    :param token:
    :param call_center_id:
    :param days_ago:
    :return:
    """
    url = "https://dialpad.com/api/v2/stats"

    payload = {
        "days_ago_end": days_ago,
        "days_ago_start": 0,
        "export_type": "stats",
        "stat_type": "recordings",
        "timezone": "UTC",
        "target_id": call_center_id,
        "target_type": "callcenter"
    }
    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "authorization": "Bearer " + token
    }

    response = requests.post(url, json=payload, headers=headers)

    if response.status_code != 200:
        # Handle error here, for now return an empty list
        return []

    data = response.json()
    request_id = data.get('request_id')

    download_url = fetch_request_result_url(token, request_id)
    csv_data = fetch_stats_csv(download_url)

    return csv_data


def extract_recording_urls(data) -> list:
    # Extract 'recording_url' from each row in the data
    recording_urls = [row['recording_url'] for row in data if 'recording_url' in row]

    return recording_urls


def fetch_request_result_url(token: str,
                             request_id: str) -> str:
    """
    Get the result, keep trying until reqyest is complete

    :param token:
    :param request_id:
    :return:
    """
    url = f"https://dialpad.com/api/v2/stats/{request_id}"

    headers = {
        "accept": "application/json",
        "authorization": f"Bearer {token}"
    }

    keep_trying = True

    data = None
    while keep_trying:
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            # Handle error here, for now return an empty list
            return ""

        data = response.json()
        status = data.get('status')

        if status == "complete" or status is None:
            keep_trying = False
        else:
            # wait a sec to see if it completes
            time.sleep(1)

    download_url = data.get('download_url')
    file_type = data.get('file_type')

    if file_type == 'csv' and download_url:
        return download_url
    else:
        return ""


def fetch_callcenters(token: str, cursor: str = None) -> list:
    """
    Fetches the whole list of call centers in the system given the current
    token, and recursevely fetches more if cursor is returned until no cursor
    is present, refer to the following api:
    https://developers.dialpad.com/reference/callcenterslistall

    :param token:
    :param cursor:
    :return:
    """

    url = "https://dialpad.com/api/v2/callcenters?limit=20"

    if cursor:
        url = url + f"?cursor={cursor}"

    headers = {
        "accept": "application/json",
        "authorization": "Bearer " + token
    }

    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        # Handle error here, for now return an empty list
        return []

    data = response.json()
    call_centers = [CallCenter(item['id'], item['name']) for item in data['items']]

    # Check if 'cursor' exists and fetch more data if necessary
    next_cursor = data.get('cursor')
    if next_cursor:
        call_centers.extend(fetch_callcenters(token, next_cursor))

    return call_centers


def fetch_stats_csv(url: str) -> list:
    """
    Fetch the csv, turn it into dicts, and return a list of dicts. Dicts
    are built from the header of the csv

    :param url:
    :return:
    """
    # Make a request to the given URL to download the CSV data
    response = requests.get(url)
    if response.status_code != 200:
        # Handle error here (e.g., raise an exception or return an empty list)
        return []

    # Use StringIO to convert the text data into a file-like object
    csv_file = StringIO(response.text)

    # Read the CSV data
    csv_reader = csv.DictReader(csv_file)

    # Store each row as a dictionary in the list
    data = [row for row in csv_reader]
    return data


def save_dict_list_as_csv(dict_list: list,
                          file_name: str) -> bool:
    """
    Takes the list of dictionaries with the recording metadata and will write
    it into a CSV.

    :param dict_list:
    :param file_name:
    :return:
    """

    if not dict_list:
        return False

    # Determine the fieldnames from the first dictionary (all dicts should have the same keys)
    fieldnames = dict_list[0].keys()

    # Write the list of dictionaries to a CSV file
    with open(file_name, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        # Write the header
        writer.writeheader()

        # Write the data rows
        for row in dict_list:
            writer.writerow(row)

    return True


def generate_filename(call_center_id: str,
                      call_center_name: str,
                      days_ago: int) -> str:
    # Get the current date
    current_date = datetime.datetime.now().strftime("%Y-%m-%d")

    # Sanitize the call center name to remove problematic characters for filenames
    sanitized_name = call_center_name.replace('/', '-')
    sanitized_name = sanitized_name.replace(' ', '-')

    # Construct the filename
    filename = f"{current_date}_ID{call_center_id}_{sanitized_name}_DaysAgo{days_ago}.csv"

    return filename


def generate_report_line(call_center_id: str,
                         call_center_name: str,
                         total_records: int) -> str:
    line = f"{call_center_id}-{call_center_name}: {total_records}\n"
    return line


def init_output_dir(output_dir) -> None:
    # Check if the directory exists
    if os.path.exists(output_dir):
        # Delete only the files in the directory
        for filename in os.listdir(output_dir):
            file_path = os.path.join(output_dir, filename)

            if os.path.isfile(file_path):
                os.remove(file_path)
    else:
        # Create the directory if it doesn't exist
        os.makedirs(output_dir)


def write_report_file(directory: str, content: str) -> None :
    current_date = datetime.datetime.now().strftime("%Y-%m-%d")
    file_name = f"rec-fetcher-report-{current_date}.txt"

    # Construct the file path
    file_path = os.path.join(directory, file_name)

    # Write the content to the file
    with open(file_path, 'w') as file:
        file.write(content)


if __name__ == '__main__':
    # load api configuration
    _ = load_dotenv(find_dotenv())  # read local .env file
    dialpad_api_key = os.getenv("DIALPAD_API_KEY")

    # setup output directory
    init_output_dir(OUTPUT_DIR)

    # store report of all the call centers processesd
    rec_fechter_run_report = ""

    # start by getting all the call centers for SonderMind
    call_centers = fetch_callcenters(dialpad_api_key)
    print(", \n".join(str(center) for center in call_centers))

    for cc in call_centers:
        print(f"Writing data for CC: {cc.name}")
        callcenter_id = cc.id
        callcenter_name = cc.name
        daysago = 900

        # for every center, get the call recording metadata, save it, and get the URLs
        url_list = fetch_recording_metadata(dialpad_api_key,
                                            callcenter_id,
                                            daysago)

        filename = generate_filename(callcenter_id,
                                     callcenter_name,
                                     daysago)

        save_dict_list_as_csv(url_list, f"{OUTPUT_DIR}{filename}")
        report_line = generate_report_line(callcenter_id,
                                           callcenter_name,
                                           len(url_list))
        rec_fechter_run_report += report_line
        print(report_line)

    write_report_file(OUTPUT_DIR, rec_fechter_run_report)


